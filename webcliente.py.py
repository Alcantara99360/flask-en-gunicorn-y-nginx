de importación de matraz , render_template, solicitud

importar redis
PalabraClave = "palabra"
DefinicionClave = "definicion"

r = redis. Redis(host='127.0.0.1', puerto=6379)
r. conjunto("id", -1)


def VerificarPalabraExistente(palabra):
    CantPalabras = r. llen(PalabraClave)
    PalabraExistente = Falso
    para i en rango(CantPalabras):
        PalabraActual = r. lindex(PalabraClave, i). decodificar('utf-8')
        if(PalabraActual == palabra):
            PalabraExistente = Verdadero
            quebrar
    volver PalabraExistente


def AgregarPalabraDef(palabra, definicion):
    r. incr("id")
    r. rpush(PalabraClave, palabra)
    r. rpush(DefinicionClave, definicion)
    print("\n palabra agregada correctamente!")


def ActualizarPalabra(AntiguaPalabra, NuevaPalabra, NuevaDefinicion):
    CantPalabras = r. llen(PalabraClave)
    para i en rango(CantPalabras):
        PalabraActual = r. lindex(PalabraClave, i). decodificar('utf-8')
        if(PalabraActual == AntiguaPalabra):
            r. lset(PalabraClave, i, NuevaPalabra)
            r. lset(DefinicionClave, i, NuevaDefinicion)
            quebrar

    imprimir("\n ! La palabra" + AntiguaPalabra + "fue actualizada!")


def EliminarPalabra(palabra):
    CantPalabras = r. llen(PalabraClave)
    para i en rango(CantPalabras):
        PalabraActual = r. lindex(PalabraClave, i). decodificar('utf-8')
        DefinicionActual = r. lindex(DefinicionClave, i). decodificar('utf-8')
        if(PalabraActual == palabra):
            r. lrem(PalabraClave, i, PalabraActual)
            r. lrem(DefinicionClave, i, DefinicionActual)
            quebrar
    print("\n ¡Palabra eliminada!")


def ShowAllWords():
    CantPalabras = r. llen(PalabraClave)
    palabras = []

    para i en rango(CantPalabras):
        palabras. anexar({"nombre": r. lindex(PalabraClave, i). descodificar(
            "utf-8"), "definicion": r. lindex(DefinicionClave, i). decodificar("utf-8")})
    volver palabras


imprimir(r. llaves())

app = Matraz(__name__)